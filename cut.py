#!/usr/bin/env python2.7

import sys
import getopt

def usage():
	print('usage: wc.py [-d DELIM -f] files ... \n \n	-d DELIM	use DELIM instead of TAB for field delimiter\n	-f FIELDS select only these FIELDS')
	return 1

def main():
	try:
		opts, args = getopt.getopt(sys.argv[1:], "d:f:")
	except getopt.GetoptError as err:
		usage()
		sys.exit(1)
	
	DEL='\t'
	D = 0
	F = 0

	for o, a in opts:
		if o == "-d":
			DEL = a
			D = 2
		elif o == "-f":
			F = 2
			fields = a
		elif o in ("-h"):
			usage()
			sys.exit()
		else:	
			assert False, "unhandled option"
	
	if (F == 0):
		print 'Need Fields'
		sys.exit()
	
	if (len(sys.argv) - D - F == 1):
		sys.argv.append('-')
	
	count=(D+F+1)
	
	file=sys.argv[count]
	
	fields = fields.split(',')
	
	if(file == '-'):
		Line = sys.stdin
		List = Line.read().split(DEL)
	else:
		Line = open(file).read()
		List = Line.split(DEL)
	
	for item in fields:	
		item = int(item)
		print List[item-1],

if __name__ == "__main__":
	main()
