#!/usr/bin/env python2.7

import sys
import getopt

def usage():
	print('usage: head.py [-n NUM] files ... \n \n 	-n NUM  print the first NUM lines instead of the first 10')
	return 1

def main():
	try:
		opts, args = getopt.getopt(sys.argv[1:], "n:h")
	except getopt.GetoptError as err:
		usage()
		sys.exit(1)
	
	num = 10
	N = 0
	
	for o, a in opts:
		if o == "-n":
			N = 2
			num = a
		elif o in ("-h"):
			usage()
			sys.exit()
		else:
			assert False, "unhandled option"
	
	num = int(num)
		
	if (len(sys.argv) - N == 1):
		sys.argv.append('-')
	
	if (N == 2):
		file = sys.argv[3]
	else:
		file = sys.argv[1]
	
	if(file == '-'):
		List = sys.stdin
		a = 0
		for line in List:
			print line,
			a += 1
			if(a == num):
				sys.exit()
	else:
		List = open(file).read().splitlines()
		a = 0
	
		for line in List:
			print line
			a += 1
			if(a == num):
				sys.exit()
	

if __name__== "__main__":
	main()
