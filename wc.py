#!/usr/bin/env python2.7

import sys
import getopt
from collections import Counter
import string

def usage():
	print("usage: wc.py [-c -l -w] files ... \n")
	print("\t-c\tprint the byte/character counts")
	print("\t-l\tprint the newline counts")
	print("\t-w\tprint the word counts\n")
def main():
	try:
		opts, args = getopt.getopt(sys.argv[1:], "clwh")
	except getopt.GetoptError as e:
		print str(e)
		usage()
		sys.exit(2)
	char = 0
	line = 0
	word = 0
	for o, a in opts:
		if o == "-c":
			char = 1
		elif o == "-l":
			line = 1
		elif o == "-w":
			word = 1
		elif o in ("-h", "--help"):
			usage()
			sys.exit()
		else:
			print("Error! You must use one of the flags:")
			usage()
			sys.exit(2)
	if len(sys.argv)-1 == 1:
		sys.argv.append('-')
	for file in sys.argv[2:]:
		if file == '-':
			stream = sys.stdin
		else:
			stream = open(file)
		if word == 1:
			with stream as f:
				words = [word for line in f for word in line.split()]
				print len(words)
		if char == 1:
			list = stream.read()
			print len(list)
		if line == 1:
			lines = sum(1 for line in stream)
			print lines
if __name__ == "__main__":
	main()
